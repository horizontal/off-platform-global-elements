;(function($){

  var Navigation = function($elem){

    var config = {
      selectors: {
        button:'.navbar-toggle'
      },
      classes: {
        collapsed:'collapsed',
        navOpen:'site-nav-open',
        bodyNavOpen:'body-nav-open'
      }
    }

    var $btn,$header,$body;

    /*
     * Listeners
     */

    var handleToggle = function(e){

      e.preventDefault();

      var $this = $(this);

      if($this.hasClass(config.classes.collapsed)) {
        $this.removeClass(config.classes.collapsed);
        $header.addClass(config.classes.navOpen);
        $body.addClass(config.classes.bodyNavOpen);
        $header.headroom('freeze');
      } else {
        $this.addClass(config.classes.collapsed);
        $header.removeClass(config.classes.navOpen);
        $body.removeClass(config.classes.bodyNavOpen);
        $header.headroom('unfreeze');
      }

    }

    /*
     * Setup Funcitons
     */


    /*
     * Utility
     */


    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $body = $('body');
      $header = $elem;
      $btn = $(config.selectors.button,$header);
    }

    var setUp = function(){
      $header.headroom({
        tolerance: 5,
        offset: $header.outerHeight(),
        classes: {
          initial: "animated",
          pinned: "slideDown",
          unpinned: "slideUp"
        }
      });
    }

    var attachListeners = function(){
      $btn.on('click',handleToggle);
    }

    return init();

  }

  $(document).ready(function(){
    $('.hztl-header').each(function(){
      new Navigation(  $(this) );
    });
  });

})(jQuery);
